# simple-audio-player :notes:
<img src="https://user-images.githubusercontent.com/70860732/113321483-8e85f880-930b-11eb-8cd9-da1b0f308243.jpeg" height="50%" width="50%" align="right">

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/simple-audio-player/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/simple-audio-player?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/simple-audio-player?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/simple-audio-player?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/simple-audio-player)](https://replit.com/@KennyOliver/simple-audio-player)

**A simple audio player based on glassmorphism**

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/simple-audio-player)](https://kennyoliver.github.io/simple-audio-player)

---
Kenny Oliver ©2021
